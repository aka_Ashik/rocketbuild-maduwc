/*global UNIFACE widget uniface AbstractWidget document _uf setImmediate UAParser */

(function() {

    class customWidget extends UNIFACE.widget.custom_widget_base {
        constructor() {
            super();
        }
        wdgt_set_value(value) {
            switch (this.element.tagName) {
     
				case "UNIFACE-CARD":
					var card_node = this.element.shadowRoot.querySelector(".data");
					card_node.innerHTML = value;
					break;
				case "UNIFACE-CARDLISTITEM":
				    var cardListItem_node = this.element.shadowRoot.querySelector(".data");
					cardListItem_node.innerHTML = value;
					break;
				case "UNIFACE-CHARTDATA":
					var Chart_UR_value,Chart_R_value;
					var month_arr = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
					for (var i = 0; i < month_arr.length ; i++ )
					{
						if (i === 0) {
							Chart_UR_value =   uniface.getInstance("RB_1054_INDEX").getEntity("CHART.NOMODEL").getOccurrence(0).getField(month_arr[i]+"_DATA_UR").getValue();
						} else {
							Chart_UR_value =   Chart_UR_value + "," + uniface.getInstance("RB_1054_INDEX").getEntity("CHART.NOMODEL").getOccurrence(0).getField(month_arr[i] + "_DATA_UR").getValue();
						}
					}
					for (var i = 0; i < month_arr.length ; i++ )
					{
						if (i === 0) {
							Chart_R_value =   uniface.getInstance("RB_1054_INDEX").getEntity("CHART.NOMODEL").getOccurrence(0).getField(month_arr[i]+"_DATA_R").getValue();
						} else {
							Chart_R_value =   Chart_R_value + "," + uniface.getInstance("RB_1054_INDEX").getEntity("CHART.NOMODEL").getOccurrence(0).getField(month_arr[i]+"_DATA_R").getValue();
						}
					}
					this.setAttributeProp("ur_data", Chart_UR_value);
				    this.setAttributeProp("r_data", Chart_R_value);
					break;
                default:
                    this.element.innerHTML = value; // Default for unknown widget type ?
            }
        }
        wdgt_get_value() {
            
        }
        wdgt_set_valrep(valrep_array) {
            if (this.element && this.element.tagName == "ION-SELECT") {
                this.element.innerHTML = "";    // Re-initialize.
                for (var i = 0; i < valrep_array.length; i++) {
                    var new_node = document.createElement("ion-select-option");
                    new_node.setAttribute("value", valrep_array[i].value);
                    new_node.innerHTML = valrep_array[i].representation;
                    this.element.appendChild(new_node);
                }
                this._delayed_valrep = null;
            } else {
                this._delayed_valrep = valrep_array;
            }

        }
        wdgt_set_style_property(property, value) {
            if (property == "color") {
                super.wdgt_set_style_property(property, "Dark"+value);
            } else {
                super.wdgt_set_style_property(property, value);
            }
        }
        wdgt_set_property(property, value) {
        }
        wdgt_map_trigger(trigger_name) {
            switch(trigger_name) {
                case "detail":
                    return {
                        element: this.element,
                        event_name: "onclick"
                    };
                case "onspecial":
                    return {
                        element: this.element,
                        event_name: "onmouseenter"
                    };
            }
        }
        wdgt_validate() {
            
        }
        wdgt_set_syntax(syntax) {
           
        }
        wdgt_on_disconnect() {
           
        }
        wdgt_on_connect() {
            
        }
    }


    // Put the class in a ACME namespace.
    if (typeof(U) == "undefined" || U == null) {
        U = {};
    }
    U.customWidget = customWidget;

}());