// FILE: ucustomwidgetbase.js

/*******************************************************************************
date   refnum   version who description
date   refnum   version who description
*******************************************************************************/
/*global UNIFACE widget document */

/*jshint es5: false */

////////////////////////////////////////////////////////////////////////////////

(function() {

    function equal_objects(obj1, obj2) {

        // Checks if obj is a non-null object.
        function is_object(obj) {
            return (typeof obj === "object" && obj != null);
        }

        // If the obj1 and obj2 refer to the exact same object, they are equal.
        if (obj1 === obj2) {
            return true;
        }
        // If either one is not an object, they are not equal.
        if (!is_object(obj1) || !is_object(obj2)) {
            return false;
        }
        // If they have a different amount of properties, they are not equal.
        if (Object.keys(obj1).length !== Object.keys(obj2).length) {
            return false;
        }
        for (var prop in obj1) {
            // If they have different values for the same property, they are not equal.
            if (!equal_objects(obj1[prop], obj2[prop])) {
                return false;
            }
        }
        return true;
    }

    class custom_widget_base extends UNIFACE.widget.element {
        constructor() {
            super();
        }
        doRender(a_placeHolder) {
            this.parent = a_placeHolder.parentNode;
            this.element = this.createElement(a_placeHolder);
            if (a_placeHolder.tagName === "UNIFACE-CHARTDATA")
            {
                this.element.id = a_placeHolder.id;
            } else {
                this.element.id = this.callBack.getId();    // first set the id
            }
            this.fillAttributes();
            this.setElementValue(this.callBack.getValue());
            this.element.style.cssText = a_placeHolder.style.cssText;
            if (this.parent) {
                this.parent.replaceChild(this.element, a_placeHolder);
            }
            this.fillStyles();
        };
        createElement(placeholder) {
            // Get the tagname property from the widget definition.
            var tag_name = this.callBack.getDefProperty("tagname");
            // If there was none, get it from the wrapper widget implementation.
            if (tag_name == undefined || tag_name == "") {
                tag_name = this.wdgt_get_tag_name();
            }

            // If no tagname is available, or if it is equal to the clone's tag name,
            // then simply return the placeholder.
            if (tag_name == undefined || tag_name == "" || tag_name == placeholder.tagName) {
                return placeholder;
            }
            // Clone the placeholder and create a new node.
            var clone = placeholder.cloneNode(true);
            var new_node = document.createElement(tag_name);
            // Move the children from the clone to the new node.
            while (clone.firstChild) {
                new_node.appendChild(clone.firstChild);
            }
            // Copy the attributes of the clone to the new node.
            for (var index = clone.attributes.length - 1; index >= 0; --index) {
                new_node.attributes.setNamedItem(clone.attributes[index].cloneNode());
            }
            // Remove the clone. We're not going to use it.
            clone.remove();
            // This widget's element will be the new node.
            return new_node;
        }
        setElementValue(value) {
            //if (this.element.id.includes("uent:") || this.element.id.includes("uocc:")) {
                // do nothing for entity and occurence widget
            //} else {
                this.wdgt_set_value(value);
            //}
        }
        getValue() {
            if (this.element.id.includes("uent:") || this.element.id.includes("uocc:")) {
                // do nothing for entity and occurence widget
            } else {
                return this.wdgt_get_value();
            }
        }
        setValrep(valrep) {
            if (!equal_objects(this._valrep, valrep)) {
                this._valrep = valrep;
                // The supplied valrep is a set of two items.
                // The first one is an object whose attribute values are the valrep
                // values and whose attribute names are the valrep representations.
                // The other item is an array that holds all representation items
                // in the defined order.
                //
                // Here, we turn this combination into a single array, where each
                // array element is an object with a "value" and a "representation"
                // property.
                var valrep_array = new Array();
                if (valrep) {
                    var valrep_pairs = valrep[0];
                    var valrep_order = valrep[1];
                    for (var i = 0; i < valrep_order.length; i++) {
                        valrep_array[i] = { value: valrep_order[i], representation: valrep_pairs[valrep_order[i]] };
                    }
                }
                // Pass that array to the custom widget.
                this.wdgt_set_valrep(valrep_array);
            }
        }
        setStyleProp(property, value) {
            return this.wdgt_set_style_property(property, value);
        }
        setHtmlProp(property, value) {
            return this.wdgt_set_html_property(property, value);
        }
        setUnifaceProp(property, value) {
            return this.wdgt_set_property(property, value);
        }
        setSyntax(syntax) {
            if (!equal_objects(this._syntax, syntax)) {
                this._syntax = syntax;
                this.wdgt_set_syntax(syntax);
            }
        }
        /*doRender(placeholder) {
            if (placeholder.id.includes("uent:") || placeholder.id.includes("uocc:") ) {
                this.element = placeholder;   // Already 'rendered'.
                this.existingClasses = this.element.className;
            } else {
                super.doRender(placeholder);
            }
        }*/
        postRender() {
            if (this.element.id.includes("uent:") || this.element.id.includes("uocc:")) {
                // do nothing for entity and occurence widget
            } else {
                super.postRender();
            }
        }
        mapEvents() {
            var events = this.callBack.getEvents();
            // Loop through the callback's events.
            for (var evt in events) if (events.hasOwnProperty(evt)) {
                // Ask the widget for a mapping for the event.
                var mapping = this.wdgt_map_trigger(evt);
                if (mapping != null) {
                    if (!mapping.element || !mapping.event_name) {
                        throw new Error("wdgt_map_trigger() returned an incomplete mapping");
                    }
                    // Register a listener, according to the returned mapping.
                    this.addListener(mapping.element, mapping.event_name, events[evt]);
                }
            }
        }
        unrender() {
            // The framework stopped rendering this widget.
            // Let the widget know that it is now disconnected.
            this.wdgt_on_disconnect();
        }
        reuse() {
            // The framework is reusing this widget.
            // Let the widget know that it is now connected.
            this.wdgt_on_connect();
        }

        ////// Default implementations for the wrapper widget API:
        wdgt_get_tag_name() {
        }
        wdgt_set_value(value) {
        }
        wdgt_get_value() {
        }
        wdgt_set_valrep(valrep_array) {
        }
        wdgt_set_style_property(property, value) {
            super.setCssProperty(this.element.style, property, value);
        }
        wdgt_set_html_property(property, value) {
            super.setHtmlProp(property, value);
        }
        wdgt_set_property(property, value) {
        }
        wdgt_map_trigger(trigger_name) {
        }
        wdgt_set_syntax(syntax) {
        }
        wdgt_on_disconnect() {
        }
        wdgt_on_connect() {
        }

    }

    UNIFACE.widget.custom_widget_base = custom_widget_base;

}());
