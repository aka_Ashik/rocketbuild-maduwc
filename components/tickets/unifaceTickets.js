const ticketTemplate = `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
<style>
* {
  margin: 0;
  padding: 0;
  list-style: none;
  text-decoration: none;
  font-family: 'Mulish', sans-serif;
  box-sizing: border-box;
}

section {
  padding: 1em;
}

div.container {
  border: 1.5px solid #DFE0EB;
  border-radius: 10px;
}

p {
  font-size: 19px;
  font-weight: 700;
  line-height: 23px;
  color: #252733;
  letter-spacing: 0.4px;
  -webkit-user-select: none;
  text-transform: none;
}

.heading {
  display: flex;
  justify-content: space-between;
  padding: 1em;
}

.heading-cta {
  display: flex;
}

.heading-cta p {
  font-size: 14px;
  font-weight: 600;
  line-height: 20px;
  color: #4B506D;
  letter-spacing: 0.2px;
  padding: 0 0.5em;
  cursor: pointer;
}

.heading-cta i {
  padding-right: 5px;
}



button.priority {
  width: fit-content;
  font-size: 11px;
  font-weight: 700;
  line-height: 13px;
  color: #FFFFFF;
  letter-spacing: 0.5px;
  text-align: center;
  text-transform: uppercase;
  border: 0;
  border-radius: 100px;
  padding: 5px 12px;
}


.low {
  background-color: #FEC400;
}

.normal {
  background-color: #29CC97;
}

.high {
  background-color: #F12B2C;
}


i {
  cursor: pointer;
  color: #C5C7CD;
}

body {
  padding: 1em;
}

.table-head,
.table-body {
  display: grid;
  grid-template-columns: 40% 20% 20% 15% 5%;
  padding: 1em;
  border-bottom: 1.5px solid #DFE0EB;
}

.table-body:last-child {
  border: 0;
}

.table-heading {
  font-size: 14px;
  font-weight: 700;
  line-height: 18px;
  color: #9FA2B4;
  letter-spacing: 0.2px;
}

.table-data {
  font-size: 14px;
  font-weight: 600;
  line-height: 20px;
  color: #252733;
  letter-spacing: 0.2px;
}
</style>

<div class="ticket-grid">
    <div class="container">
      <div class="heading">
        <p class="heading-text">All tickets</p>
        <div class="heading-cta">
          <p><i class="fa-solid fa-arrow-up-wide-short"></i>Sort
          <p><i class="fa-solid fa-filter"></i>Filter</p>
        </div>
      </div>
      <div class="table">
        <div class="table-head">
          <h3 class="table-heading">Ticket details</h3>
          <h3 class="table-heading">Customer name</h3>
          <h3 class="table-heading">Date</h3>
          <h3 class="table-heading">Priority</h3>
          <h3></h3>
        </div>
        <slot></slot> 
      </div>
    </div>
  </div>
`;

class UnifaceTickets extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.innerHTML = ticketTemplate;

		// Declaring references to the DOM elements from the template
		this.heading = this.shadowRoot.querySelector(".tic-head");
		this.name = this.shadowRoot.querySelector(".cus-head");
		this.date = this.shadowRoot.querySelector(".date-head");
		this.priority = this.shadowRoot.querySelector(".priority");
	}
}

customElements.define("uniface-tickets", UnifaceTickets);
