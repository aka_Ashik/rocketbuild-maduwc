const cardTemplate = `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
<style>
  * {
    margin: 0;
    padding: 0;
    list-style: none;
    text-decoration: none;
    font-family: 'Mulish', sans-serif;
    box-sizing: border-box;
  }

  .card {
    border: 1px solid #DFE0EB;
    border-radius: 8px;
    /* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);*/
    /* transition: 0.3s; */
    /*width: 200px;*/
  }

  .card:hover {
    /* box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);*/
    cursor: pointer;
  }

  .card.clicked {
    outline: 2px solid #3751FF;
    cursor: pointer;
  }

  .card.clicked h4,
  .card.clicked p {
    color: #3751FF;
  }

  .container {
    padding: 14px 22px;
    text-align: center;
  }

  h4 {
    font-size: 40px;
    font-weight: 700;
    line-height: 50px;
    color: #252733;
    -webkit-user-select: none;
  }

  p {
    font-size: 19px;
    font-weight: 700;
    line-height: 23px;
    color: #9FA2B4;
    letter-spacing: 0.4px;
    -webkit-user-select: none;
  }
</style>

<div class="card">
  <div class="container">
    <p class="heading"></p>
    <h4 class="data"><b></b></h4>
  </div>
</div>
`;

class UnifaceCard extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.innerHTML = cardTemplate;

		// Declaring references to the DOM elements from the template
		this.card = this.shadowRoot.querySelector(".card");
		this.heading = this.shadowRoot.querySelector(".heading");
		this.data = this.shadowRoot.querySelector(".data");

		// Select card onclick
		this.card.addEventListener("click", (e) => {
			this.card.classList.add("clicked");
			e.stopPropagation();
		});

		// Deselect card onclick outside
		document.addEventListener("click", (e) => {
			if (e.target != this.card) {
				this.card.classList.remove("clicked");
			}
		});
	}

	static get observedAttributes() {
		return ["heading", "data"];
	}

	attributeChangedCallback(attribute, oldVal, newVal) {
		switch (attribute) {
			case "heading":
				this.heading.innerText = newVal;
				break;
			case "data":
				this.data.innerText = newVal;
				break;
		}
	}
}

customElements.define("uniface-card", UnifaceCard);
