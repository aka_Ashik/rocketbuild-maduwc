class UnifaceChartData extends HTMLElement {
	constructor() {
		super();
		this.ur_data;
		this.r_data;
	}

	connectedCallback() {
		this.innerHTML = `
      <canvas id="myChart" width="" height="500"></canvas>
    `;
		this.render();
	}

	render() {
		// Render Starts
		const labelVal = [
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December",
		];
		const datas = {
			labels: labelVal,
			datasets: [
				{
					label: "Unresolved",
					data: this.ur_data,
					fill: true,
					borderColor: "rgba(55, 81, 255, 1)",
					backgroundColor: "rgba(55, 81, 255, 0.08)",
					tension: 0.4,
					showLine: true,
					boxHeight: 10,
					boxWidth: 10,
				},
				{
					label: "Resolved",
					data: this.r_data,
					fill: false,
					borderColor: "rgba(223, 224, 235, 1)",
					tension: 0.4,
					showLine: true,
				},
			],
		};
		const ctx = document.getElementById("myChart").getContext("2d");
		const myChart = new Chart(ctx, {
			type: "line",
			data: datas,
			options: {
				title: {
					display: false,
				},
				responsive: true,
				maintainAspectRatio: false,
				scales: {
					y: {
						position: "right",
					},
					x: {
						grid: {
							display: false,
						},
					},
				},
			},
		});
		// Render Ends
	}

	static get observedAttributes() {
		return ["ur_data", "r_data"];
	}

	attributeChangedCallback(attribute, oldVal, newVal) {
		switch (attribute) {
			case "ur_data":
				this.ur_data = newVal.split(",");
				break;
			case "r_data":
				this.r_data = newVal.split(",");
				break;
		}
	}
}

customElements.define("uniface-chartdata", UnifaceChartData);
