const templates = `
<style>

  * {
      margin: 0;
      padding: 0;
      list-style: none;
      text-decoration: none;
      font-family: 'Mulish', sans-serif;
      box-sizing: border-box;
  }

  .grid {
    display: grid;
    grid-template-columns: auto;
    border: 1px solid #DFE0EB;
    border-radius: 8px;
    /* width: 750px; */
  }

  .trend-info {
    padding: 1em 1em;
  }

  .date-info .date-cta{
    display: flex;
    justify-content: space-between;
  }

  .date-cta div {
    display: flex;
  }

  .date-cta div p {
    font-weight: 600;
  }

  .date-cta p {
    font-size: 12px;
    font-weight: 400;
    line-height: 16px;
    color: #9FA2B4;
  }

  .info-list .info-item {
    font-size: 16px;
    font-weight: 600;
    text-align: center;
    line-height: 22px;
    color: #9FA2B4;
    border-bottom: 1px solid;
    border-left: 1px solid;
    padding: 2.2em 0em;
  }

  .info-item:last-child {
    border-bottom: none ;
  }

  .info-list .info-item p{
    font-size: 24px;
    font-weight: 700;
    text-align: center;
    line-height: 24px;
    color: #252733;
  }

</style>

<div class="grid">
<div class="trend-info">
  <div class="date-info">
    <span>Ticket Statistics</span>
    <div class="date-cta">
      <p class="date-val"></p>
    </div>
  </div>
  <div class="graph-info">
    <slot name="chart"></slot>
  </div>
</div>
</div>
`;

class UnifaceChart extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.innerHTML = templates;
	}
}

customElements.define("uniface-chart", UnifaceChart);
