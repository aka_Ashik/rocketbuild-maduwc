const cardListTemplate = `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
<style>
    * {
      margin: 0;
      padding: 0;
      list-style: none;
      text-decoration: none;
      font-family: 'Mulish', sans-serif;
      box-sizing: border-box;
    }

    .card {
      border: 1px solid #DFE0EB;
      border-radius: 8px;
      width: 540px;
    }

    .container {
      padding: 32px 32px;
      text-align: center;
    }

    .heading-cta {
      display: grid;
      grid-template-columns: auto auto;
      justify-content: space-between;
    }

    p.heading {
      font-size: 19px;
      font-weight: 700;
      line-height: 23px;
      color: #252733;
      letter-spacing: 0.4px;
    }

    p.sub-heading {
      font-size: 12px;
      font-weight: 400;
      line-height: 16px;
      color: #9FA2B4;
      letter-spacing: 0.1px;
      text-align: left;
    }

    a {
      font-size: 14px;
      font-weight: 600;
      line-height: 20px;
      color: #3751FF;
      letter-spacing: 0.2px;
      cursor: pointer;
    }

    .list-item {
      padding-top: 1em;
    }

    .list-item ul li {
      display: grid;
      grid-template-columns: 15px 220px 25px 65px;
      grid-column-gap: 2em;
      border-bottom: 1px solid #DFE0EB;
      padding: 1em 0;
    }

    li p {
      text-align: left;
    }

    li p.data {
      text-align: right;
    }

    input#checkbox {
      height: 20px;
      width: 20px;
    }

    button {
      padding: 5px 12px;
      border: 0;
      border-radius: 8px;
    }
  </style>

  <div class="card">
    <div class="container">
      <div class="heading-cta">
        <p class="heading"></p>
        <a href="#">View details</a>
        <p class="sub-heading"></p>
      </div>
      <div class="list-item">
        <ul>
          <slot></slot>
        </ul>
      </div>
    </div>
  </div>
`;

class UnifaceCardList extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.innerHTML = cardListTemplate;

		// Declaring references to the DOM elements from the template
		this.card = this.shadowRoot.querySelector(".card");
		this.heading = this.shadowRoot.querySelector(".heading");
		this.subheading = this.shadowRoot.querySelector(".sub-heading");
		this.linkVal = this.shadowRoot.querySelector("a");
	}

	static get observedAttributes() {
		return ["heading", "subheading", "link"];
	}

	attributeChangedCallback(attribute, oldVal, newVal) {
		switch (attribute) {
			case "heading":
				this.heading.innerText = newVal;
				break;
			case "subheading":
				this.subheading.innerText = newVal;
				break;
			case "link":
				this.linkVal.innerText = newVal;
				break;
		}
	}
}

customElements.define("uniface-cardlist", UnifaceCardList);
