const cardListItemTemplate = `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
<style>
    * {
      margin: 0;
      padding: 0;
      list-style: none;
      text-decoration: none;
      font-family: 'Mulish', sans-serif;
      box-sizing: border-box;
    }

    li {
      display: grid;
      grid-template-columns: 15px 220px 25px 65px;
      grid-column-gap: 2em;
      border-bottom: 1px solid #DFE0EB;
      padding: 1em 0;
      justify-content: space-between;
    }

    li p {
      text-align: left;
    }

    li p.data {
      text-align: right;
    }

    input#checkbox {
      height: 20px;
      width: 20px;
    }

    button {
      padding: 5px 12px;
      border: 0;
      border-radius: 8px;
    }

    button.danger {
      background-color:  #F12B2C;
      color:white;
    }
    button.warning {
      background-color:  #FEC400;
      color:white;
    }
    button.success {
      background-color:  #29CC97;
      color:white;
    }
  </style>
      <li>
        <input type="checkbox" name="checkbox" id="checkbox">
        <p class="taskheading"></p>
        <p class="data"></p>
        <button id="task-btn"></button>
      </li>

`;

class UnifaceCardListItem extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.innerHTML = cardListItemTemplate;

		// Declaring references to the DOM elements from the template
		this.list = this.shadowRoot.querySelector("li");
		this.inputControl = this.shadowRoot.querySelector("#checkbox");
		this.taskheading = this.shadowRoot.querySelector(".taskheading");
		this.data = this.shadowRoot.querySelector(".data");
		this.btn = this.shadowRoot.querySelector("#task-btn");
	}

	connectedCallback() {
		if (!this.hasAttribute("buttontext")) {
			this.btn.style.display = "none";
			this.list.style.gridTemplateColumns = "250px auto";
		} else {
			this.btn.style.display = "block";
			this.list.style.gridTemplateColumns = "15px 220px 25px 65px";
		}
	}

	static get observedAttributes() {
		return ["heading", "data", "buttontext", "buttoncolor", "showinput"];
	}

	attributeChangedCallback(attribute, oldVal, newVal) {
		switch (attribute) {
			case "heading":
				this.taskheading.innerText = newVal;
				break;
			case "data":
				this.data.innerText = newVal;
				break;
			case "buttontext":
				console.log(newVal);
				if (newVal === "") {
					this.btn.style.display = "none";
				} else {
					this.btn.style.display = "block";
					this.btn.innerText = newVal;
				}

				break;
			case "buttoncolor":
				this._changeColor(newVal);
				break;
			case "showinput":
				if (newVal === "false") {
					console.log(newVal);
					this.inputControl.style.display = "none";
				} else {
					console.log(newVal);
					this.inputControl.style.display = "block";
				}
				break;
		}
	}

	_changeColor(value) {
		this.btn.className = "";
		this.btn.classList.add(value);
	}
}

customElements.define("uniface-cardlistitem", UnifaceCardListItem);
