class UnifaceNavitem extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.innerHTML = `
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
    <style>

      * {
      margin: 0;
      padding: 0;
      list-style: none;
      text-decoration: none;
      font-family: 'Mulish', sans-serif;
      box-sizing: border-box;
      }

      li a {
        display: block;
        height: 100%;
        width: 100%;
        font-weight: 400;
        font-size: 16px;
        text-align: left;
        line-height: 20px;
        color:#A4A6B3;
        cursor: pointer;
        padding-left: 48px;
        box-sizing: border-box;
        transition: 0.3s;
      }

      li hr {
        color: #DFE0EB;
        background-color: #DFE0EB;
        opacity: 0.06;
        line-height: 0;
      }

      li a:hover {
        color: #DDE2FF;
      }

      li a:active,
      li a:focus {
        color: #DFE0EB;
      }

      li a i {
        margin-right: 12px;
        height: 24px;
        width: 24px;
      }

    </style>
    
    <li class="nav-item">
      <a href="#">
        <i class="fa-solid "></i>
        <slot></slot>
      </a>
    </li>
    `;

		// Declaring references to the DOM elements
		this.listNode = this.shadowRoot.querySelector("li");
		this.linkNode = this.shadowRoot.querySelector("a");
		this.iconNode = this.shadowRoot.querySelector("i");
	}

	static get observedAttributes() {
		return ["icon", "link"];
	}

	attributeChangedCallback(attribute, oldVal, newVal) {
		switch (attribute) {
			case "icon":
				this._addIcon(newVal);
				break;
			case "link":
				this.linkNode.href = newVal;
				break;
		}
	}

	_addIcon(value) {
		this.iconNode.classList.add("fa-" + value);
	}
}

customElements.define("uniface-navitem", UnifaceNavitem);
