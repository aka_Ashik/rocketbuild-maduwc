const template = `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
  <style>
    * {
      margin: 0;
      padding: 0;
      list-style: none;
      text-decoration: none;
      font-family: 'Mulish', sans-serif;
      box-sizing: border-box;
    }

    .sidenav {
      position: fixed;
      top: 0;
      left: 0;
      width: 255px;
      height: 100%;
      background: #363740;
    }

    .sidenav header {
      padding: 40px 0px; 
      font-size: 20px;
      font-weight: 700;
      text-align: center;
      color: #A4A6B3;
      line-height: 24px;
      opacity: 0.7;
      user-select: none;
    }

    .sidenav .nav-list {
      display: flex;
      flex-direction: column;
      gap: 1em;
    }

    .sidenav ul hr {
      color: #DFE0EB;
      background-color: #DFE0EB;
      opacity: 0.06;
      line-height: 0;
    }
   
  </style>

  <div class="sidenav">
    <header class="nav-brand"></header>
    <ul class="nav-list">
      <slot name="nav-item"></slot>
      <hr />
      <slot name="nav-item2"></slot>
    </ul>
  </div>
`

class UnifaceNavbar extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.shadowRoot.innerHTML = template;
    
    // Declaring references to the DOM elements from the template
    this.brandName = this.shadowRoot.querySelector("header");
  }

  static get observedAttributes() {
    return ['brand'];
  }

  attributeChangedCallback(attribute, oldVal, newVal) {

    switch(attribute) {
      case "brand":
        this.brandName.innerText = newVal;
        break;
    }
  }
} 

customElements.define('uniface-navbar',UnifaceNavbar);